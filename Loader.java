import java.io.PrintWriter;
import java.io.StringWriter;
import java.lang.reflect.Method;
import java.net.URL;
import java.net.URLClassLoader;

public class Loader
{
    public static void main(String[] args)
    {
        try
        {
            URL[] urls = new URL[] { new URL("file:///tmp/foo/server.jar"), new URL("file:///tmp/foo/serverapi.jar") };
            URLClassLoader urlClassLoader = new URLClassLoader(urls, Loader.class.getClassLoader());

            ClassLoader oldClassLoader = Thread.currentThread().getContextClassLoader();

            try
            {
                Thread.currentThread().setContextClassLoader(urlClassLoader);

                invokeAllHellos(urlClassLoader,
                                    "ServerAPI.ServerAPI1",
                                    "ServerAPI.ServerAPI2",
                                    "Server.Server1",
                                    "Server.Server2");
            }
            finally
            {
                Thread.currentThread().setContextClassLoader(oldClassLoader);
            }
        }
        catch (Throwable t)
        {
            System.out.println("Got exception: " + t);
        }
    }

    private static void invokeAllHellos(ClassLoader classLoader, String... classnames)
    {
        for (String classname : classnames)
        {
            try
            {
                System.out.println("\n\n===================\n\nTrying to load class " + classname + ":\n");
                Class clazz = classLoader.loadClass(classname);

                System.out.println("Constructing new object for class " + classname);

                Object classInstance = clazz.newInstance();

                Method m = clazz.getMethod("hello");

                System.out.println("Calling hello from class " + classname);
                m.invoke(classInstance);
            }
            catch (Exception e)
            {
                System.out.println("Got error " + e.toString() + " loading class name " + classname + ": " +
                                    getPrintableStackTrace(e));
            }
        }
    }

    public static String getPrintableStackTrace(Throwable t)
    {
        StringWriter sw = new StringWriter();
        t.printStackTrace(new PrintWriter(sw));

        return sw.toString();
    }

}
