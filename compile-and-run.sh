#!/bin/bash
rm Loader.class Server/*.class ServerAPI/*.class
javac *.java Server/*.java ServerAPI/*.java
mkdir /tmp/foo
rm /tmp/foo/server.jar /tmp/foo/serverapi.jar
jar cf /tmp/foo/server.jar Server/Server?.class
jar cf /tmp/foo/serverapi.jar ServerAPI/ServerAPI?.class
cp Loader.class /tmp/foo
pushd /tmp/foo
java -cp . Loader
popd

